using System;

namespace branchmerger
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			BranchMergerParameters branchmergerparameters;
			if (!ParametersParser.parse (args, out branchmergerparameters)) {
				Console.WriteLine (ParametersParser.getInformations());
				return;
			}
			if (branchmergerparameters.SrcDirs == null && !branchmergerparameters.OverlayfsMode) {
				return;
			}

			if (((branchmergerparameters.LowerDirs == null) || (branchmergerparameters.UpperDirs == null) )&& branchmergerparameters.OverlayfsMode) {
				return;
			}

			if (string.IsNullOrEmpty (branchmergerparameters.TargetDir)) {
				return;
			}

			var mergerprocessor = new BranchMergerProcessor (branchmergerparameters);
			mergerprocessor.Proceed ();
		}
	}
}
