using System;
using System.IO;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Mono.Unix;
using Mono.Unix.Native;
using System.Linq;
using System.Security.Cryptography;
using System.Collections;

namespace branchmerger
{
	public class BranchMergerProcessor
	{
		private BranchMergerParameters branchmergerparameters;
		public BranchMergerProcessor (BranchMergerParameters branchmergerparameters)
		{
			this.branchmergerparameters = branchmergerparameters;
		}

		private void changeUnixFileInfo(string filename,Dictionary<string,UnixUserInfo> localusers ,Dictionary<string,UnixGroupInfo> localgroups)
		{
			var unixfileinfo = new UnixFileInfo(filename);

			var owneruserid = unixfileinfo.OwnerUserId;
			var ownergroupid = unixfileinfo.OwnerGroupId;
			if (!string.IsNullOrEmpty(branchmergerparameters.User))  {
				owneruserid = localusers[branchmergerparameters.User].UserId;
			}

			if (!string.IsNullOrEmpty(branchmergerparameters.Group))  {
				ownergroupid = localgroups[branchmergerparameters.Group].GroupId;
			}

			if ((owneruserid != unixfileinfo.OwnerUserId) || (ownergroupid != unixfileinfo.OwnerGroupId)){

				Mono.Unix.Native.Syscall.chown(filename,(uint) owneruserid,(uint) ownergroupid);
			}


			if (!string.IsNullOrEmpty(branchmergerparameters.Mode))  {

				FilePermissions mode = (FilePermissions) Convert.ToInt32(branchmergerparameters.Mode,8);

				Mono.Unix.Native.Syscall.chmod(filename,mode);
			}
		}
		private byte[] GetHash(string filename)
		{
			byte[] result = null;
			using (var md5 = MD5.Create())
			{
				using (var stream= File.OpenRead(filename))
				{
					result = md5.ComputeHash(stream);
				}
			}
			return result;
		}

		private void ProceedUnionfs()
		{
			var localusers = UnixUserInfo.GetLocalUsers ().ToDictionary (x => x.UserName, x => x);
			var localgroups = UnixGroupInfo.GetLocalGroups ().ToDictionary (x => x.GroupName, x => x);

			var branchselements = new ConcurrentDictionary<string,HashSet<string>> ();
			branchmergerparameters.SrcDirs.AsParallel ().ForAll (x =>
			                                                     {
				var files = Directory.GetFiles (x,"*.*",SearchOption.AllDirectories);

				branchselements.TryAdd(x,new HashSet<string>(files.Select(y=> y.Remove(0,x.Length))));
			});

			var allkeys = branchselements.Keys.ToArray ();
			HashSet<string> hashset;

			if (!branchselements.TryGetValue (allkeys [0], out hashset))
				return;

			HashSet<string> hashset2;
			var keys = allkeys.Where (x => x != allkeys [0]).ToArray ();

			foreach (var key in keys) {
				if (!branchselements.TryGetValue (key, out hashset2))
					continue;
				hashset.IntersectWith (hashset2);
			}

			var filestoproceed = new ConcurrentBag<string> ();
			hashset.AsParallel ().ForAll (x =>
			                              {
				byte[] hashvalue = null;
				foreach (var key in allkeys)
				{
					using (var md5 = MD5.Create())
					{
						var filename = Path.Combine(key,x);
						using (var stream= File.OpenRead(filename))
						{
							var _hashvalue = md5.ComputeHash(stream);
							if (hashvalue==null) {
								_hashvalue =hashvalue;
								continue;
							}
							if (StructuralComparisons.StructuralEqualityComparer.Equals(hashvalue,_hashvalue)) 
								return;
						}
					}
				}
				filestoproceed.Add(x);
			});

			var lockobject = new object ();
			filestoproceed.AsParallel().ForAll (x =>
			                                    {
				var fullfilename = Path.Combine(branchmergerparameters.TargetDir,x);
				var directoryname = Path.GetDirectoryName(fullfilename);
				if (!branchmergerparameters.Pretend) {
					lock (lockobject)
					{
						if (!Directory.Exists(directoryname)) {
							Directory.CreateDirectory(directoryname);
							changeUnixFileInfo(directoryname,localusers,localgroups);
						}
					}
				}

				bool bcopied=false;
				foreach (var key in allkeys) {
					var filename = Path.Combine(key,x);
					if (!bcopied) {
						if (!branchmergerparameters.Pretend) {
							if (!branchmergerparameters.SilentMode) {
								Console.WriteLine("[COPY]\t"+ filename+"\t->"+fullfilename);
							}
							File.Copy(filename,fullfilename,true);
							changeUnixFileInfo(fullfilename,localusers,localgroups);
						}
						else {
							Console.WriteLine("[PRETEND]\t"+ filename+"\t->"+fullfilename);
						}
						bcopied = true;
					}
					if (!branchmergerparameters.Pretend) {
						if (branchmergerparameters.CleanBranch) {
							File.Delete(filename);
						}
					}
				}
			});

		}

		private void MergeUpperLowerFiles (ConcurrentDictionary<string,HashSet<string>> upperfiles, ConcurrentDictionary<string,HashSet<string>> lowerfiles)
		{
			var upperdirs = upperfiles.Keys;
			var lowerdirs = lowerfiles.Keys;
			foreach (var upperdir in upperdirs) {
				var files = upperfiles [upperdir];
				foreach (var lowerdir in lowerdirs) {
					var _files = lowerfiles[lowerdir];
					foreach (var file in files)
					{
						if (_files.Contains (file)) {
							_files.Remove (file);
						}
					}
				}
			}
		}

		private void RemoveDuplications(string[]dirs, ConcurrentDictionary<string,HashSet<string>> dirsfiles)
		{
			var masterdir = dirs.First ();
			var masterfiles = dirsfiles [masterdir];

			foreach (var file in masterfiles) {
				foreach (var dir in dirs.Where(x=>string.Compare(x,masterdir)!=0))
				{
					if (!dirsfiles [dir].Contains (file)) {
						continue;
					}
					dirsfiles [dir].Remove (file);
				}
			}

		}

		private ConcurrentDictionary<string,HashSet<string>>  CombineUpperLowerFiles(ConcurrentDictionary<string,HashSet<string>> upperfiles, ConcurrentDictionary<string,HashSet<string>> lowerfiles)
		{
			var result = new ConcurrentDictionary<string,HashSet<string>> ();
			foreach (var element in upperfiles) {
				result.TryAdd (element.Key, element.Value);
			}
			foreach (var element in lowerfiles) {
				result.TryAdd (element.Key, element.Value);
			}
			return result;
		}

		private void ProceedOverlayfs()
		{
			var localusers = UnixUserInfo.GetLocalUsers ().ToDictionary (x => x.UserName, x => x);
			var localgroups = UnixGroupInfo.GetLocalGroups ().ToDictionary (x => x.GroupName, x => x);

			var upperfiles = new ConcurrentDictionary<string,HashSet<string>> ();
			branchmergerparameters.UpperDirs.AsParallel ().ForAll (x =>
			                                                     {
				var files = Directory.GetFiles (x,"*.*",SearchOption.AllDirectories);

				upperfiles.TryAdd(x,new HashSet<string>(files.Select(y=> y.Remove(0,x.Length))));
			});

			var lowerfiles = new ConcurrentDictionary<string,HashSet<string>> ();
			branchmergerparameters.LowerDirs.AsParallel ().ForAll (x =>
			                                                       {
				var files = Directory.GetFiles (x,"*.*",SearchOption.AllDirectories);

				lowerfiles.TryAdd(x,new HashSet<string>(files.Select(y=> y.Remove(0,x.Length))));
			});

			RemoveDuplications (branchmergerparameters.UpperDirs, upperfiles);
			RemoveDuplications (branchmergerparameters.LowerDirs, lowerfiles);

			MergeUpperLowerFiles (upperfiles, lowerfiles);

			var combinedfiles = CombineUpperLowerFiles(upperfiles, lowerfiles);
			var filetoproceed = new ConcurrentBag<Tuple<string,string>> ();
			foreach (var element in combinedfiles) {
				element.Value.AsParallel ().ForAll (file => {
					var destfilename = Path.Combine (branchmergerparameters.TargetDir, file);
					var srcfilename = Path.Combine (element.Key, file);
					if (File.Exists (destfilename)) {
						var _hashvalue = GetHash (destfilename);
						var hashvalue = GetHash (srcfilename);
						if (StructuralComparisons.StructuralEqualityComparer.Equals(hashvalue,_hashvalue)) 
							return;
					}
					filetoproceed.Add(new Tuple<string,string>(element.Key,file));
				});
			}
			var lockobject = new object ();
			filetoproceed.AsParallel ().ForAll (x =>
			{
				var fullfilename = Path.Combine (branchmergerparameters.TargetDir, x.Item2);
				var directoryname = Path.GetDirectoryName(fullfilename);
				var filename = Path.Combine (x.Item1, x.Item2);
				lock(lockobject)
				{
					if (!Directory.Exists(directoryname)) {
						Directory.CreateDirectory(directoryname);
						changeUnixFileInfo(directoryname,localusers,localgroups);
					}
				}

				if (!branchmergerparameters.Pretend) {
					if (!branchmergerparameters.SilentMode) {
						Console.WriteLine("[COPY]\t"+ filename+"\t->"+fullfilename);
					}
					File.Copy(filename,fullfilename,true);

					changeUnixFileInfo(fullfilename,localusers,localgroups);
				}
				else {
					Console.WriteLine("[PRETEND]\t"+ filename+"\t->"+fullfilename);
				}


			});
		}




		public void Proceed()
		{
			if (branchmergerparameters.OverlayfsMode) {
				ProceedOverlayfs ();
			} 
			else {
				ProceedUnionfs ();
			}
		}
	}
}

