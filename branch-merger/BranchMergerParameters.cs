using System;

namespace branchmerger
{
	public class BranchMergerParameters{
		public string[] SrcDirs { get; set; }
		public string TargetDir {get;set;}
		public bool SilentMode{ get; set;}
		public string User { get; set; }
		public string Group { get; set; }
		public string Mode { get; set; }
		public bool Pretend {get;set;}
		public bool CleanBranch {get;set;}
		public bool OverlayfsMode {get;set;}
		public string[] UpperDirs {get;set;}
		public string[] LowerDirs {get;set;}

	}
}

