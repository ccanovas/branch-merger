using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;

namespace branchmerger
{
	public class ParametersParser
	{
		private enum ParserStatus {
			None,
			Options

		}
		private static string getVersion()
		{
			var version = System.Reflection.Assembly.GetExecutingAssembly ().GetName ().Version;
			return string.Format ("{0}.{1}", version.Major, version.MajorRevision);
		}

		private static string getApplicationName()
		{

			return System.Reflection.Assembly.GetExecutingAssembly ().GetName().Name;
		}

		private static Dictionary<string,string> parseOptions(string[] options)
		{
			var result = new Dictionary<string,string> ();
			foreach (var option in options) {
				var values = option.Split (new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
				if (values.Length  > 1) {
					result.Add (values [0].Trim(), values [1]);
				} else {
					result.Add (values [0].Trim(), null);
				}
			}
			return result;
		}

		public static bool parse(string[] args,out BranchMergerParameters branchmergerparameters)
		{
			bool result = false;
			branchmergerparameters = new BranchMergerParameters ();
			var parserstatus = ParserStatus.None;

			foreach (var _arg in args) {
				result = true;
				if (_arg == "-o") {
					parserstatus = ParserStatus.Options;
					continue;
				}
				if (_arg == "-v") {
					Console.WriteLine (getVersion ());
					break;
				}
				switch (parserstatus) {

					case ParserStatus.Options:
					var options = _arg.Split (new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
					var optionsdico = parseOptions (options);
					foreach (var keyvalue in optionsdico) {
						switch (keyvalue.Key) {
							case "overlayfsmode":
							branchmergerparameters.OverlayfsMode = true;
							break;
							case "upperdirs":
							branchmergerparameters.UpperDirs = keyvalue.Value.Split (new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries).Select (x => x.EndsWith (Path.DirectorySeparatorChar.ToString ()) ? x : x + Path.DirectorySeparatorChar.ToString ()).ToArray ();
							break;
							case "lowerdirs":
							branchmergerparameters.LowerDirs = keyvalue.Value.Split (new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries).Select (x => x.EndsWith (Path.DirectorySeparatorChar.ToString ()) ? x : x + Path.DirectorySeparatorChar.ToString ()).ToArray ();
							break;
							case "cleanbranch":
							branchmergerparameters.CleanBranch = true;
							break;
							case "pretend":
							branchmergerparameters.Pretend = true;
							break;
							case "silentmode":
							branchmergerparameters.SilentMode = true;
							break;
							case "br":
							branchmergerparameters.SrcDirs = keyvalue.Value.Split (new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries).Select (x => x.EndsWith (Path.DirectorySeparatorChar.ToString()) ? x :x+Path.DirectorySeparatorChar.ToString() ).ToArray ();
							break;
							case "user":
							branchmergerparameters.User = keyvalue.Value;
							break;
							case "group":
							branchmergerparameters.Group = keyvalue.Value;
							break;
							case "mode":
							branchmergerparameters.Mode = keyvalue.Value;
							break;
							default:
							break;
						}
					}
					parserstatus = ParserStatus.None;
					break;
					default:
					branchmergerparameters.TargetDir = _arg;
					break;

				}
			}

			return result;
		}

		public static string getInformations()
		{
			//var result =@"aufs-merger v 1.0
			var str = new StringBuilder ();
			str.AppendLine (getApplicationName()+"\t" + getVersion ());
			str.AppendLine("Mono version\t"+System.Reflection.Assembly.GetExecutingAssembly ().ImageRuntimeVersion);
			str.AppendLine ("-v\tShow version");
			str.AppendLine ("-o [silentmode],[overlayfsmode],[cleanbranch],[pretend],[upperdirs=path1:path2...],[lowerdirs=path1:path2...],[br=path1:path2...],[user=],[group=],[mode=] /destination");
			return str.ToString ();
		}
	}
}

